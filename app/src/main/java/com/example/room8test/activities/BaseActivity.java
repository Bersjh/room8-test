package com.example.room8test.activities;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;

import com.example.room8test.R;

public abstract class BaseActivity extends ActionBarActivity {
    private ProgressDialog progressDialog;
    private boolean needToRestoreProgressDialog;
    private int progressTextResId = -1;

    @Override
    protected void onStart() {
        super.onStart();
        restoreProgressDialogIfNeeded();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveProgressDialogState();
    }

    private void saveProgressDialogState() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            needToRestoreProgressDialog = true;
        }
    }

    private void restoreProgressDialogIfNeeded() {
        if (needToRestoreProgressDialog) {
            needToRestoreProgressDialog = false;
            if (progressTextResId != -1) {
                showProgressDialog(progressTextResId);
            } else {
                showProgressDialog();
            }
        }
    }

    public void showProgressDialog() {
        showProgressDialog(R.string.default_progress_dialog_text);
    }

    public void showProgressDialog(int textResId) {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressTextResId = textResId;
            progressDialog = ProgressDialog.show(this, "", getString(progressTextResId), true);
        }
    }

    public void hideProgressDialog() {
        needToRestoreProgressDialog = false;
        progressTextResId = -1;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
