package com.example.room8test.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.room8test.Constants;
import com.example.room8test.R;
import com.example.room8test.adapter.CompaniesAdapter;
import com.example.room8test.api.ApiManager;
import com.example.room8test.db.provider.CompaniesProvider;
import com.example.room8test.db.provider.FriendsProvider;
import com.example.room8test.fragments.CompaniesFragment;
import com.example.room8test.model.dto.CompanyDto;
import com.example.room8test.model.dto.FriendDto;
import com.example.room8test.model.tables.CompaniesTable;
import com.example.room8test.model.tables.FriendsTable;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class CompaniesListActivity extends BaseActivity implements CompaniesAdapter.IOpenFriendsListListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_container_activity);

        Fragment companiesListFragment = new CompaniesFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, companiesListFragment);
        fragmentTransaction.commit();

        if (!isDbCreated()) { //if db is empty load data from web
            showProgressDialog(R.string.loading_data_progress_dialog_text);
            ApiManager apiManager = new ApiManager();
            apiManager.loadCompaniesData(new LoadingCallback());
        }
    }

    private boolean isDbCreated() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                Constants.SharedPreferences.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(Constants.SharedPreferences.SHARED_PREFERENCES_KEY_IS_DB_CREATED, false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openFriendsList(int companyId) {
        Intent intent = new Intent(this, FriendsListActivity.class);
        intent.putExtra(Constants.Extras.EXTRA_KEY_COMPANY_ID, companyId);
        startActivity(intent);
    }

    /**
     * Will be called by Retrofit once data loading finished
     */
    private class LoadingCallback implements Callback<List<CompanyDto>> {

        @Override
        public void success(List<CompanyDto> companyDtos, Response response) {
            Log.d(Constants.LOG_TAG, "Success");
            ArrayList<ContentValues> companiesCv = new ArrayList<>();
            ArrayList<ContentValues> friendsCv = new ArrayList<>();
            int currentCompanyId = 1;
            for (CompanyDto companyDto : companyDtos) {
                ContentValues companyCv = new ContentValues();
                companyCv.put(CompaniesTable.GUID, companyDto.getGuid());
                companyCv.put(CompaniesTable.ADDRESS, companyDto.getAddress());
                companyCv.put(CompaniesTable.BALANCE, companyDto.getBalance());
                companyCv.put(CompaniesTable.EMAIL, companyDto.getEmail());
                companyCv.put(CompaniesTable.PHONE, companyDto.getPhone());
                companyCv.put(CompaniesTable.NAME, companyDto.getName());

                companiesCv.add(companyCv);

                for (FriendDto friendDto : companyDto.getFriends()) {
                    ContentValues friendCv = new ContentValues();
                    friendCv.put(FriendsTable.NAME, friendDto.getName());
                    friendCv.put(FriendsTable.COMPANY_ID, currentCompanyId);
                    friendsCv.add(friendCv);
                }
                currentCompanyId++;
            }
            ContentResolver contentResolver = getContentResolver();
            contentResolver.bulkInsert(CompaniesProvider.CONTENT_URI, companiesCv.toArray(new ContentValues[companiesCv.size()]));
            contentResolver.bulkInsert(FriendsProvider.CONTENT_URI, friendsCv.toArray(new ContentValues[friendsCv.size()]));

            SharedPreferences sharedPreferences = getSharedPreferences(
                    Constants.SharedPreferences.SHARED_PREFERENCES_NAME,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(Constants.SharedPreferences.SHARED_PREFERENCES_KEY_IS_DB_CREATED, true);
            editor.apply();

            hideProgressDialog();
        }

        @Override
        public void failure(RetrofitError error) {
            Log.e(Constants.LOG_TAG, "Error", error);
            hideProgressDialog();
        }
    }
}
