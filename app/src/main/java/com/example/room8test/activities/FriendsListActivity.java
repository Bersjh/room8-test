package com.example.room8test.activities;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.room8test.Constants;
import com.example.room8test.R;
import com.example.room8test.fragments.FriendsFragment;

public class FriendsListActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_container_activity);

        int companyId = getIntent().getIntExtra(Constants.Extras.EXTRA_KEY_COMPANY_ID, 0);
        Fragment friendsListFragment = FriendsFragment.getInstance(companyId);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, friendsListFragment);
        fragmentTransaction.commit();
    }
}
