package com.example.room8test.model.tables;

import android.database.sqlite.SQLiteDatabase;

public class CompaniesTable extends BaseTable {
    public static final String TABLE_NAME = "companies";

    public static final String GUID = "guid";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";
    public static final String BALANCE = "balance";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement, "
            + NAME + " text not null, "
            + GUID + " text not null,"
            + EMAIL + " text,"
            + ADDRESS + " text,"
            + PHONE + " text, "
            + BALANCE + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
