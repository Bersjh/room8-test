package com.example.room8test.model.tables;


public abstract class BaseTable {
    public static final String ID = "_id";
    public static final String NAME = "name";
}
