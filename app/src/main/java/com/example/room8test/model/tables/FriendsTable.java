package com.example.room8test.model.tables;

import android.database.sqlite.SQLiteDatabase;

public class FriendsTable extends BaseTable {
    public static final String TABLE_NAME = "friends";

    public static final String COMPANY_ID = "company_id";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement, "
            + NAME + " text not null, "
            + COMPANY_ID + " integer not null, "
            + "foreign key(" + COMPANY_ID + ") references " + CompaniesTable.TABLE_NAME + "(" + CompaniesTable.ID + ") on delete cascade "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
