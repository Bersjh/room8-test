package com.example.room8test.api;


import com.example.room8test.Constants;
import com.example.room8test.model.dto.CompanyDto;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

public interface ITest {

    @GET(Constants.Urls.GET_COMPANIES_URL)
    public void getCompanies(Callback<List<CompanyDto>> callback);
}
