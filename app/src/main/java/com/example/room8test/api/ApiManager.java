package com.example.room8test.api;

import com.example.room8test.Constants;
import com.example.room8test.model.dto.CompanyDto;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class ApiManager {
    private ITest testAdapter;

    public ApiManager() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setLongSerializationPolicy(LongSerializationPolicy.STRING);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.Urls.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(gsonBuilder.create()))
                .build();

        testAdapter = restAdapter.create(ITest.class);
    }

    public void loadCompaniesData(Callback<List<CompanyDto>> callback) {
        testAdapter.getCompanies(callback);
    }
}
