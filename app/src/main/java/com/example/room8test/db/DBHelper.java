package com.example.room8test.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.room8test.Constants;
import com.example.room8test.model.tables.CompaniesTable;
import com.example.room8test.model.tables.FriendsTable;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        try {
            CompaniesTable.onCreate(database);
            FriendsTable.onCreate(database);
        } catch (SQLException e) {
            Log.e(Constants.LOG_TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        try {
            CompaniesTable.onUpgrade(database, oldVersion, newVersion);
            FriendsTable.onUpgrade(database, oldVersion, newVersion);
        } catch (SQLException e) {
            Log.e(Constants.LOG_TAG, "Error deleting database", e);
        }
    }
}
