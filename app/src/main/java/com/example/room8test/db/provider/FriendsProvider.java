package com.example.room8test.db.provider;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.example.room8test.model.tables.FriendsTable;


public class FriendsProvider extends BaseProvider {
    private static final String AUTHORITY = "com.example.room8test.db.provider.friends";

    private static final int FRIENDS = 1;
    private static final int FRIEND_ID = 2;
    private static final int COMPANY_ID = 3;

    private static final String BASE_PATH = "friends";
    private static final String PATH_BY_COMPANY_ID = BASE_PATH + "/by_company";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    public static final Uri CONTENT_URI_BY_COMPANY_ID = Uri.parse("content://" + AUTHORITY + "/" + PATH_BY_COMPANY_ID);
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, FRIENDS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", FRIEND_ID);
        uriMatcher.addURI(AUTHORITY, PATH_BY_COMPANY_ID + "/#", COMPANY_ID);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // Set the table
        queryBuilder.setTables(FriendsTable.TABLE_NAME);

        int uriType = uriMatcher.match(uri);
        switch (uriType) {
            case FRIENDS:
                break;
            case FRIEND_ID:
                queryBuilder.appendWhere(FriendsTable.ID + "=" + uri.getLastPathSegment());
                break;
            case COMPANY_ID:
                queryBuilder.appendWhere(FriendsTable.COMPANY_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = uriMatcher.match(uri);
        SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case FRIENDS:
                id = sqlDB.insert(FriendsTable.TABLE_NAME, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    protected String getTableNameForInsert() {
        return FriendsTable.TABLE_NAME;
    }
}
