package com.example.room8test.fragments;


import android.app.Activity;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.room8test.Constants;
import com.example.room8test.adapter.CompaniesAdapter;
import com.example.room8test.db.provider.CompaniesProvider;
import com.example.room8test.model.tables.CompaniesTable;

public class CompaniesFragment extends BaseListFragment {
    private CompaniesAdapter.IOpenFriendsListListener openFriendsListListener;

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            openFriendsListListener = (CompaniesAdapter.IOpenFriendsListListener) activity;
        } catch (ClassCastException e) {
            Log.e(Constants.LOG_TAG, "CompaniesFragment should be attached to activity that implements IOpenFriendsListListener interface");
            throw e;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(new CompaniesAdapter(getActivity(), null, openFriendsListListener));
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri contentUri = CompaniesProvider.CONTENT_URI;
        String[] projection = new String[]{CompaniesTable.ID, CompaniesTable.NAME, CompaniesTable.ADDRESS,
                CompaniesTable.PHONE, CompaniesTable.EMAIL, CompaniesTable.BALANCE, CompaniesTable.GUID};

        return new CursorLoader(getActivity(), contentUri, projection, null, null, null);
    }
}
