package com.example.room8test.fragments;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.room8test.Constants;
import com.example.room8test.adapter.FriendsAdapter;
import com.example.room8test.db.provider.FriendsProvider;
import com.example.room8test.model.tables.FriendsTable;


public class FriendsFragment extends BaseListFragment {

    public static FriendsFragment getInstance(int companyId) {
        FriendsFragment friendsFragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.Extras.EXTRA_KEY_COMPANY_ID, companyId);
        friendsFragment.setArguments(args);
        return friendsFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(new FriendsAdapter(getActivity(), null));
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int companyId = getArguments().getInt(Constants.Extras.EXTRA_KEY_COMPANY_ID);
        Uri contentUri = FriendsProvider.CONTENT_URI;
        String[] projection = new String[]{FriendsTable.ID, FriendsTable.NAME};
        String where = FriendsTable.COMPANY_ID + " = " + companyId;

        return new CursorLoader(getActivity(), contentUri, projection, where, null, null);
    }
}
