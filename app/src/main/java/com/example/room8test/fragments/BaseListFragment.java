package com.example.room8test.fragments;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.widget.CursorAdapter;


public abstract class BaseListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        getListAdapter().swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public CursorAdapter getListAdapter() {
        return (CursorAdapter) super.getListAdapter();
    }
}
