package com.example.room8test.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.room8test.R;
import com.example.room8test.model.tables.CompaniesTable;

/**
 * Populates companies info
 */
public class CompaniesAdapter extends CursorAdapter {
    private LayoutInflater inflater;
    private IOpenFriendsListListener openFriendsListListener;

    public CompaniesAdapter(Context context, Cursor cursor, IOpenFriendsListListener openFriendsListListener) {
        super(context, cursor, true);
        inflater = LayoutInflater.from(context);
        this.openFriendsListListener = openFriendsListListener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.list_item_company, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtGuid = (TextView) view.findViewById(R.id.txt_guid);
        TextView txtName = (TextView) view.findViewById(R.id.txt_name);
        TextView txtEmail = (TextView) view.findViewById(R.id.txt_email);
        TextView txtAddress = (TextView) view.findViewById(R.id.txt_address);
        TextView txtPhone = (TextView) view.findViewById(R.id.txt_phone);
        TextView txtBalance = (TextView) view.findViewById(R.id.txt_balance);

        final int companyId = cursor.getInt(cursor.getColumnIndex(CompaniesTable.ID));

        txtGuid.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.GUID)));
        txtName.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.NAME)));
        txtEmail.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.EMAIL)));
        txtAddress.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.ADDRESS)));
        txtPhone.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.PHONE)));
        txtBalance.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.BALANCE)));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFriendsListListener.openFriendsList(companyId);
            }
        });
    }

    public interface IOpenFriendsListListener {
        public void openFriendsList(int companyId);
    }
}
