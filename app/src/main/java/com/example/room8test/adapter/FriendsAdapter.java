package com.example.room8test.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.room8test.model.tables.CompaniesTable;

/**
 * Populates friends info
 */
public class FriendsAdapter extends CursorAdapter {
    private LayoutInflater inflater;

    public FriendsAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtName = (TextView) view.findViewById(android.R.id.text1);

        txtName.setText(cursor.getString(cursor.getColumnIndex(CompaniesTable.NAME)));
    }
}
