package com.example.room8test;

public final class Constants {
    private Constants() {
    }

    public final static String LOG_TAG = "Room8Test";
    public final static String DATABASE_NAME = "test_db";

    public final static class SharedPreferences {
        public static final String SHARED_PREFERENCES_NAME = "testPrefs";
        public final static String SHARED_PREFERENCES_KEY_IS_DB_CREATED = "is_db_created";
    }

    public final static class Urls {
        public static final String BASE_URL = "http://json-generator.appspot.com/api/json/get";
        public static final String GET_COMPANIES_URL = "/cbHNXtQQeq?indent=2";
    }

    public final static class Extras {
        public static final String EXTRA_KEY_COMPANY_ID = "company_id";
    }
}
