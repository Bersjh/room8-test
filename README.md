# README #

You can build this project using [Gradle](https://gradle.org/). If you don't know how to do it, use the following command:

```
#!bash

./gradlew assembleDebug
```